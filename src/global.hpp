#ifndef GLOBAL_HPP
#define GLOBAL_HPP

#include <SDL.h>
#include "state.hpp"
#include "types.hpp"
#include "gfx.hpp"
#include "nfont/NFont.h"
#include <list>

using namespace std;


// Mostly non-game-specific stuff
struct GlobalState {
    SDL_Window* window;
    SDL_Renderer* renderer;

    XY screensize;
    XY map_pos;
    XY hud_pos;
    XY hud_size;

    // Number of seconds this frame
    double stepSec;

    bool paused;

    // The topleft of the viewport, measured in units
    XYd camera;
    double scale;  // pixels per unit
    XYd tilesize;  // units?

    GraphicalState graphical;

    // Resources
    SpriteSheet *tileset0;
    SpriteSheet *spritesets;

    NFont guifont;
    NFont chatfont;

    GlobalState() {
        camera = XYd{{0.0}, {0.0}};
        scale = 1.0;
        paused = false;
        tileset0 = NULL;
        spritesets = NULL;
    }

    ~GlobalState() {
        delete tileset0;
        delete spritesets;
    }
};

extern GlobalState gstate;

const char nl = '\n';


#endif
