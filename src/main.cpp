#include "stdio.h"
#include "math.h"
#include "global.hpp"
#include "state.hpp"
#include <vector>

using namespace std;

GlobalState gstate;


/************************************ Drawing *********************************/


// Return the largest non-positive double congruent to x modulo y
double fmodneg(double x, double y) {
    double ret = fmod(x, y);
    if (ret > 0.0)
        ret -= y;
    return ret;
}

/*
void drawTrails() {
    // Pixel position of topleft viewport corner
    XYd cameraOffset = gstate.scale * gstate.camera - XYd(gstate.screensize) * 0.5;

    vector<SDL_Point> trail;
    for (auto trailIt = it->trail.begin(); trailIt != it->trail.end(); ++trailIt) {
        trail.push_back(SDL_Point(gstate.scale * *trailIt - cameraOffset));
    }

    SDL_SetRenderDrawColor(gstate.renderer, 0, 0, 255, 255);
    SDL_RenderDrawLines(gstate.renderer, &trail[0], trail.size());
}
*/

/*************************************  Logic *********************************/


void calcFPS(double frameSec, SpriteSheet *&fpsText) {
    static double fpsIntervalLen = 0.0;
    static int fpsIntervalFrames = 0;

    fpsIntervalFrames++;
    if ( (fpsIntervalLen += frameSec) >= 1.0 ) {
        //printf("fps: %.1f\n", fpsIntervalFrames / fpsIntervalLen);
        delete fpsText;
        fpsText = renderText(gstate.guifont, 300, formatText("fps: %.1f\n", fpsIntervalFrames / fpsIntervalLen));

        fpsIntervalLen = 0.0;
        fpsIntervalFrames = 0;
    }
}

void gameloop() {
    int numkeys;
    uint8_t* keystate = SDL_GetKeyboardState(&numkeys);
    if (!keystate) {
        fprintf(stderr, "no keyboard state\n");
        return;
    }

    uint32_t lastticks = SDL_GetTicks();


    //SpriteSheet *text1 = renderText(gstate.guifont, 300, "Hello there!\naaaaaa aaaaa aaa aa a aaaa aaaaa aaaa aaaff fffaaaa");
    //SpriteSheet *text2 = renderText(gstate.chatfont, 300, "Texting, etc");
    SpriteSheet *fpsText = new SpriteSheet();

    while (1) {
        uint32_t ticks = SDL_GetTicks();
        double frameSec = (ticks - lastticks) * 0.001;
        lastticks = ticks;

        calcFPS(frameSec, fpsText);

        gstate.stepSec = min(frameSec, 0.2);

        // event handling
        SDL_Event e;
        if (SDL_PollEvent(&e)) {

            if (e.type == SDL_QUIT)
                break;

            else if (e.type == SDL_KEYUP) {
                if (e.key.keysym.sym == SDLK_ESCAPE)
                    break;
                else if (e.key.keysym.sym == SDLK_F4 && (e.key.keysym.mod & KMOD_ALT))
                    break;
                else if (e.key.keysym.sym == SDLK_p)
                    gstate.paused ^= true;
            }
        }

        // double camSpeed = gstate.stepSec * 1000 / gstate.scale;
        // if (keystate[SDL_SCANCODE_DOWN])  gstate.camera.y += camSpeed;
        // if (keystate[SDL_SCANCODE_UP])    gstate.camera.y -= camSpeed;
        // if (keystate[SDL_SCANCODE_LEFT])  gstate.camera.x -= camSpeed;
        // if (keystate[SDL_SCANCODE_RIGHT]) gstate.camera.x += camSpeed;

        if (!gstate.paused) {
        }

        // clear the screen
        SDL_SetRenderDrawColor(gstate.renderer, 0, 0, 0, 255);
        SDL_RenderClear(gstate.renderer);

        // Draws everything, including HUD elements, except the FPS
        gstate.graphical.draw();

        fpsText->drawAt(gstate.screensize - XY{110, 20});

        SDL_RenderPresent(gstate.renderer);

        //FIXME: Should only wait if vsync disabled
        SDL_Delay(5);
    }
}

void mainmenu() {

    gstate.graphical.generate_map();

    gameloop();
}

int main() //int argc, char* argv[])
{

    // Initialize SDL.
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        return 1;

#ifdef _WIN32
    freopen("stdout.txt", "w", stdout);
    freopen("stdout.txt", "w", stderr);
#endif

    gstate.screensize = XY{1270, 940};  // 1275x920
    gstate.map_pos = {5, 0};
    gstate.hud_pos = {5, MAPHEIGHT * 20 + 5};
    gstate.hud_size = gstate.screensize - gstate.hud_pos;

    gstate.tilesize = XY{20, 20};

    // Create the window where we will draw.
    gstate.window = SDL_CreateWindow("Mutant Sludge Zone",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED, //SDL_WINDOWPOS_CENTERED,
                              gstate.screensize.w, gstate.screensize.h,
                              SDL_WINDOW_SHOWN | SDL_WINDOW_MAXIMIZED);

    // We must call SDL_CreateRenderer in order for draw calls to affect this window.
    gstate.renderer = SDL_CreateRenderer(gstate.window, -1, SDL_RENDERER_PRESENTVSYNC);

    // Load resources
    gstate.tileset0 = new SpriteSheet(loadTexture("Thespazztikone_tilemaps_000.bmp", true), 16, 10);
    gstate.spritesets = new SpriteSheet(loadTexture("blueslime.bmp", true), 25, 1, XY{0, 20});
    gstate.chatfont.load(loadSurface("GreenVenus26.bmp"));
    gstate.guifont.load(loadSurface("LtBlueTwCen18Border.bmp"));//"BlueAgency20.bmp"));

    mainmenu();

    SDL_Quit();
    return 0;
}
