#ifndef TYPES_HPP
#define TYPES_HPP

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iterator>
#include <SDL_rect.h>

template<typename iter_t>
inline iter_t incPtr(iter_t it, int amount = 1) {
    iter_t ret = it;
    std::advance(ret, amount);
    return ret;
}

template<typename Value>
struct Point {
    union { Value x, w; };
    union { Value y, h; };

    Point() : x(0), y(0) {}
    Point(Value _x, Value _y) : x(_x), y(_y) {}
    template<typename Value2>
    Point(Point<Value2> pt) : x(pt.x), y(pt.y) {}

    bool zero() const {
        return !x && !y;
    }

    double abs() const {
        return sqrt((double)(x * x + y * y));
    }

    Value abs2() const {
        return x * x + y * y;
    }

    Value dot(const Point<Value> &rhs) const {
        return x * rhs.x + y * rhs.y;
    }

    // Assuming collinear with rhs, return t such that lhs = t * rhs
    double collinearDivide(const Point<Value> &rhs) const {
        if (fabs(x) > fabs(y))
            return (double)x / rhs.x;
        return (double)y / rhs.y;
    }

    // Projection onto rhs
    Point<double> projOnto(const Point<Value> &rhs) const {
        return this->dot(rhs) * rhs / (double)rhs.abs2();
    }

    Point<double> norm() const {
        return *this / abs();
    }

    //X and Y are treated as increasing to bottom right
    Point<Value> clockwise90() const {
        return Point<Value>(-y, x);
    }

    Point<Value> anticlockwise90() const {
        return Point<Value>(y, -x);
    }

    // Magnitude of the cross product when treated as 3-vectors
    Value orthogonalProduct(const Point<Value> &rhs) const {
        return x * rhs.y - y * rhs.x;
    }

    Point<Value> operator-() const {
        return Point<Value>(-x, -y);
    }

    Point<Value> operator+(const Point<Value> &rhs) const {
        return Point<Value>(x + rhs.x, y + rhs.y);
    }

    Point<Value> operator+=(const Point<Value> &rhs) {
        return (*this = *this + rhs);
    }

    Point<Value> operator-(const Point<Value> &rhs) const {
        return Point<Value>(x - rhs.x, y - rhs.y);
    }

    Point<Value> operator*(Value rhs) const {
        return Point<Value>(x * rhs, y * rhs);
    }

    Point<Value> operator*=(Value rhs) {
        return (*this = *this * rhs);
    }

    Point<Value> operator/(Value rhs) const {
        return Point<Value>(x / rhs, y / rhs);
    }

    Point<Value> operator/=(Value rhs) {
        return (*this = *this / rhs);
    }

    operator SDL_Point() const {
        return SDL_Point{int(x), int(y)};
    }

    template<typename Value2>
    friend std::ostream &operator<<(std::ostream &os, const Point<Value2> &rhs);
    template<typename Value2>
    friend Point<Value2> operator*(Value2 lhs, const Point<Value2> &rhs);
};

typedef Point<int> XY;
typedef Point<double> XYd;

inline std::ostream &operator<<(std::ostream &os, const XY &rhs) {
    return os << rhs.x << ", " << rhs.y;
}

inline std::ostream &operator<<(std::ostream &os, const XYd &rhs) {
    return os << rhs.x << ", " << rhs.y;
}

inline XY operator*(int lhs, const XY &rhs) {
    return XY(lhs * rhs.x, lhs * rhs.y);
}

inline XYd operator*(double lhs, const XYd &rhs) {
    return XYd(lhs * rhs.x, lhs * rhs.y);
}

#endif
