#ifndef STATE_HPP
#define STATE_HPP

#include <memory>
#include <stdint.h>
#include <vector>
#include <set>
#include "gfx.hpp"

using namespace std;


#define MAPWIDTH   63
#define MAPHEIGHT  44
// Number of door types/switches - each can trigger multiple doors
#define MAX_DOOR_TYPES  16

// Cooldown ticks
#define HERO_MOVE_COOLDOWN 2
#define HERO_SHOOT_COOLDOWN 2
#define HERO_LASER_COOLDOWN 3
#define HERO_BOMB_COOLDOWN 5

#define HERO_MAX_HP 50

////////////////////////////////// Direction


enum Direction {
    INVALID = -1,
    UP, UPRIGHT, RIGHT, DOWNRIGHT, DOWN, DOWNLEFT, LEFT, UPLEFT,
};

extern const Direction cardinal_dirs[4];

int sign(int x);
int dirx(Direction dir);
int diry(Direction dir);
Direction xy_to_dir(int x, int y);
// Return distance times 10
int dirdist10(Direction dir);


/*
  Basic state
*/
/***************************** Simplified Game State **************************/


// bits?
enum PlayerAction {
    actNone,
    actMove,
    actShoot,
    actShootLaser,
    actBomb,
//    actOperate,   // A door switch
};


////////////////////////////////// BasicTiles

// Used in BasicTile
enum BasicTileEnum {
    btileFloor,
    btileWall,
    btilePit,
    btileAutodoor,
    btileRemoteDoor,  // data holds door num
    btileDoorSwitch,  // data holds door num. Impassable
    btileExit,

    // No limit
};

struct BasicTile {
    BasicTileEnum type;
    int8_t data;

    void set(BasicTileEnum type_, int8_t data_ = 0) {
        type = type_;
        data = data_;
    }

    // Doors count as passable, because there if a bobjDoor on
    // the tile if the door is closed
    bool hero_passable() const { return type != btileWall && type != btilePit && type != btileDoorSwitch;  }
    // Returns false for the exit so enemies don't step on it
    bool enemy_passable() const { return type != btileWall && type != btilePit && type != btileDoorSwitch && type != btileExit;  }
    bool fire_passable() const { return enemy_passable(); }
    // Can a box be pushed into/on it?
    bool box_passable() const { return type != btileWall && type != btileDoorSwitch && type != btileExit;  }
    bool bullet_passable() const { return type != btileWall && type != btileDoorSwitch;  }
};


////////////////////////////////// BasicObjects

// Used in BasicObject
enum BasicObjectEnum {
    bobjNone = 0,

    //bobjPlayer = 1,  // Indicated only by hero.x/y
    // The following all use data as number of ticks until next move
    bobjGreySlime = 2,
    bobjBlueSlime = 3,
    bobjGreenSlime = 4,
    bobjRedSlime1 = 5,   // Has 1 HP
    bobjRedSlime2 = 6,   // Has 2 HP

    bobjGlass = 7,
    bobjDoor = 8,        // A closed door; have to check immutable state to see whether an autodoor

    bobjFire = 9,        // data holds strength 1-15
    bobjBox = 10,

    bobjAmmo = 11,       // data is num bullets / 2
    bobjHealth = 12,     // health pack, data is hp cured
    bobjBomb = 13,       // just one bomb
    bobjLaserAmmo = 14,  // data is num bullets

    // Limited to 16 total
    bobjUnused = 15
};

struct BasicObject {
    //uint8_t type:4;   // Of type BasicObjectEnum
    BasicObjectEnum type:4;
    uint8_t data:4;

    void set(BasicObjectEnum type_, uint8_t data_ = 0) {
        type = type_;
        data = data_;
    }

    // decrement .data, saturated at 0.
    int decrement(int amount) {
        data = max(0, (int)data - amount);
        return data;
    }
};


////////////////////////////////// MutableState

class ImmutableState;

// Stores the positions of objects and entities on the map,
// plus non
class MutableState {
private:
    // This constructor should only ever be used to initialised
    // ImmutableState.initial_state.
    MutableState();

public:
    MutableState(ImmutableState *imstate_);

    BasicObject objects[MAPWIDTH][MAPHEIGHT];

    BasicObject *objects_begin() { return &objects[0][0]; }
    BasicObject *objects_end() { return objects_begin() + MAPWIDTH * MAPHEIGHT; }

    const BasicTile &tile(int x, int y);


    // Contains stuff that may be saved and restored by the AI,
    // like which doors are open and other modifiable map state
    struct Hero {
        int8_t x, y;  // Also mirrored in objects
        int16_t hp, ammo, laser_ammo, bombs;
        int8_t shoot_cooldown;
        int8_t move_cooldown;
        //bool have_boulder;
        bool can_shoot();
        bool can_shoot_laser();
        uint8_t valid_actions();  // returns a bitmask of PlayerActions
        // Don't need to record player direction here!
    } hero;

    const ImmutableState *imstate;

    // Enemy counts
    uint16_t greys, blues, greens, reds;

    uint16_t ticknum;
    uint32_t prng_state;

    uint32_t rng(int range);  // uniformly generate in [0,range - 1]

    uint8_t sensible_fire_directions(); // returns bitmask of 8 directions 
    uint8_t sensible_bomb_directions(); // returns bitmask of 8 directions 

    void step(PlayerAction action = actNone, Direction action_dir = INVALID);  // must be actNone if not player turn
    bool is_player_turn();
    bool won();
    bool lost();
    //vector<uint8_t> to_array();

protected:
    // This should only be called on an empty tile
    void spawn(int x, int y, BasicObjectEnum type);
private:
    // This should only be called on sludge
    void kill(int x, int y);

    void trigger_switch(int doornum);
    void maybe_close_autodoor(int x, int y);
    void maybe_trigger_door(int x, int y);

    void move_player_to(int x, int y);

    void player_action(PlayerAction action, Direction dir);

    void player_move(Direction dir);
    void player_shoot(Direction dir, bool laser);
    void player_bomb(Direction dir);
    void explode_bomb_at(int x, int y);

    void slime_logic(int x, int y);
    bool slime_try_move(int x, int y, Direction dir);
    void grey_slime_maybe_morph(BasicObject &obj, int x, int y);

    friend class ImmutableState;
};


////////////////////////////////// ImmutableState

class ImmutableState {
    // Contains map walls and initial state, level number, etc
    //graph<Objective> objectives;
public:
    ImmutableState();

    BasicTile tiles[MAPWIDTH][MAPHEIGHT];

    BasicTile *tiles_begin() { return &tiles[0][0]; }
    BasicTile *tiles_end() { return tiles_begin() + MAPWIDTH * MAPHEIGHT; }

    int level_num;
    const char *level_name;

protected:
    // Used by the map generator

    // True if there is a RemoteDoor at this location
    // that is initially open. Other doors and tiles ignored
    //bool doors_initially_open[MAPWIDTH][MAPHEIGHT];
    // Aka next unused door type id
    int num_door_types;

    // Initial state
    MutableState initial_state;

    // Hero initial_hero_state;

    friend class MutableState;
    friend class GraphicalState;
};

inline const BasicTile &MutableState::tile(int x, int y) { return imstate->tiles[x][y]; }


/*********************************** AI Stuff *********************************/


class AIMutableState {
    // Wraps MutableState with some stuff only needed in A* nodes
    MutableState state;
    //turn number, hash, objectives and heuristics, prev action, pointer to previous macronode solution (stores entire action sequence)
};

class MacroNode {
    // What's the objective, and the objective function
    MacroNode *parent;
    // Parent MacroNode. action sequence since parent. 
    vector<PlayerAction> actions;
    MutableState state; //?
};


/****************************** Graphical Game State **************************/

// Position of a sprite on the map
struct MapSprite {
    int x, y, z;
    //shared_ptr<SpriteSheet> spritesheet;
    int frame;
    SpriteSheet *spritesheet;  // Not owned

    void draw();

    bool operator<(const MapSprite &rhs) const { return y < rhs.y; }
};

enum RoomStyle {
    stylePlain,
};

// This is used only for human players, and wraps the *State classes
// to provide sound effects, replay recording, graphics, etc.
class GraphicalState {
public:
    GraphicalState();
    ~GraphicalState();

    void draw();

    // Functions in mapgen.cpp
    void generate_map();

private:
    void draw_map(int layer);
    void update_hud();

    // Functions in mapgen.cpp
    void generate_toplevel();
    void place_room(XY topleft, XY size, RoomStyle style);
    //void place_enemy(int x, int y, BasicObjectEnum type);

    void put_floor(int x, int y, RoomStyle style);
    void put_wall(int x, int y, RoomStyle style);
    void floor_rect(XY topleft, XY size, RoomStyle style);

    ImmutableState *imstate;
    MutableState *mustate;

    //set<MapObject> objects;

    RoomStyle stylemap[MAPWIDTH][MAPHEIGHT];

    // Layer 0 contains floor, walls, and other tiles.
    // Layer 1 contains overhead tiles, the tops of walls.
    int tilemap[MAPWIDTH][MAPHEIGHT][2];
    Direction player_dir;

    SpriteSheet *hud;
};


#endif
