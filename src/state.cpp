#include "state.hpp"
#include <stdlib.h>   // abs
#include <algorithm>  // min, max
#include <assert.h>
#include <queue>

using namespace std;


Direction const cardinal_dirs[4] = {
    UP, RIGHT, DOWN, LEFT
};

/*
int dirx(Direction dir) {
    if (dir == LEFT) return -1;
    if (dir == RIGHT) return 1;
    return 0;
}

int diry(Direction dir) {
    if (dir == UP) return -1;
    if (dir == DOWN) return 1;
    return 0;
}
*/

static const int dir_x_components[] = {
    0, 1, 1, 1, 0, -1, -1, -1
};

static const int dir_y_components[] = {
    -1, -1, 0, 1, 1, 1, 0, -1
};

static const Direction signs_to_dirs[3][3] = {
    {UPLEFT, LEFT, DOWNLEFT},    // x < 0
    {UP, INVALID, DOWN},              // x = 0
    {UPRIGHT, RIGHT, DOWNRIGHT}  // x > 0
};


int sign(int x) {
    if (x < 0)
        return -1;
    if (x > 0)
        return 1;
    return 0;
}

int dirx(Direction dir) {
    return dir_x_components[dir];
}

int diry(Direction dir) {
    return dir_y_components[dir];
}

Direction xy_to_dir(int x, int y) {
    return signs_to_dirs[1 + sign(x)][1 + sign(y)];
}

// Return distance times 10
int dirdist10(Direction dir) {
    if (dir & 1) return 14;
    return 10;
}


/*********************************** Basic State ******************************/

ImmutableState::ImmutableState() {
    // make empty
    fill(tiles_begin(), tiles_end(), BasicTile{btileFloor, 0});
    num_door_types = 0;
    level_num = -1;
    level_name = "undefined";
    // bool *doors_begin = &doors_initially_open[0][0];
    // fill(doors_begin, doors_begin + MAPWIDTH * MAPHEIGHT, false);

    // Zero out initial state
}

MutableState::MutableState() {
    // There are no virtual methods or RTTI so this should be safe.
    // Want to do a memset of the whole structure rather than individually
    // zeroing everything to ensure there are no uninitialised bytes that
    // would affect hashing.
    memset(this, 0, sizeof(MutableState));

    //fill(objects_begin(), objects_end(), bobjNone);
}

MutableState::MutableState(ImmutableState *imstate_) {
    *this = imstate_->initial_state;
    imstate = imstate_;
    // hero = imstate->initial_hero_state;
    // // Place doors
    // for (int x = 0; x < MAPWIDTH; x += 1) {
    //     for (int y = 0; y < MAPHEIGHT; y += 1) {
    //         if
    //         if (imstate->doors_initially_open
    //     }
    // }
}

bool MutableState::won() {
    return tile(hero.x, hero.y).type == btileExit && hero.hp > 0;
}

bool MutableState::lost() {
    // You can only land on a pit tile by being pushed
    return hero.hp <= 0 || tile(hero.x, hero.y).type == btilePit;
}


bool MutableState::Hero::can_shoot() {
    return ammo > 0 && shoot_cooldown == 0;
}

bool MutableState::Hero::can_shoot_laser() {
    return laser_ammo > 0 && shoot_cooldown == 0;
}


uint32_t MutableState::rng(int range) {
// 32 bit int, low bits are very poor
    prng_state = (prng_state * 1103515245 + 12345);
    return (prng_state >> 16) % range;
}


// This should only be called on sludge
void MutableState::kill(int x, int y) {
    BasicObject &obj = objects[x][y];
    switch (obj.type) {
        case bobjGreySlime:
            greys--;
            break;
        case bobjBlueSlime:
            blues--;
            break;
        case bobjGreenSlime:
            greens--;
            break;
        case bobjRedSlime1:
        case bobjRedSlime2:
            reds--;
            break;
        default:
            assert(false);
    }
    obj.set(bobjNone);
}

// This should only be called on an empty tile
void MutableState::spawn(int x, int y, BasicObjectEnum type) {
    assert(tile(x, y).type == btileFloor);
    switch (type) {
        case bobjGreySlime:
            greys++;
            break;
        case bobjBlueSlime:
            blues++;
            break;
        case bobjGreenSlime:
            greens++;
            break;
        case bobjRedSlime1:
        case bobjRedSlime2:
            reds++;
            break;
        default:
            assert(false);
    }
    BasicObject &obj = objects[x][y];
    assert(obj.type == bobjNone);
    obj.set(type, 0);
}

// void MutableState::close_door(int x, int y) {

// }

// void MutableState::open_door(int x, int y) {

// }

void MutableState::maybe_close_autodoor(int x, int y) {
    if (tile(x, y).type == btileAutodoor && objects[x][y].type == bobjNone) {
        objects[x][y].set(bobjDoor);
    }
}

void MutableState::maybe_trigger_door(int x, int y) {
    if (tile(x, y).type == btileAutodoor && objects[x][y].type == bobjDoor) {
        objects[x][y].set(bobjNone);
    }
    if (tile(x, y).type == btileDoorSwitch) {
        trigger_switch(tile(x, y).data);
    }
}

void MutableState::trigger_switch(int doornum) {
    for (int x = 0; x < MAPWIDTH; x += 1) {
        for (int y = 0; y < MAPHEIGHT; y += 1) {
            if (tile(x, y).type == btileRemoteDoor && tile(x, y).data == doornum) {
                BasicObject &obj = objects[x][y];
                switch (obj.type) {
                    case bobjNone:
                    case bobjFire:
                        obj.set(bobjDoor);
                        break;
                    case bobjDoor:
                        obj.set(bobjNone);
                        break;
                    case bobjGreySlime:
                    case bobjBlueSlime:
                    case bobjGreenSlime:
                    case bobjRedSlime1:
                    case bobjRedSlime2:
                        // Kills
                        kill(x, y);
                        obj.set(bobjDoor);
                        break;
                    case bobjBox:
                        // Door is jammed and can't close
                        break;
                    default:
                        // All others should be impossible
                        assert(false);
                }
            }
        }
    }
}


/*********************************** Player Actions ***************************/


void MutableState::move_player_to(int x, int y) {
    // Update doors
    // First close all previous adjacent autodoors
    maybe_close_autodoor(hero.x, hero.y + 1);
    maybe_close_autodoor(hero.x, hero.y - 1);
    maybe_close_autodoor(hero.x - 1, hero.y);
    maybe_close_autodoor(hero.x + 1, hero.y);

    //objects[hero.x][hero.y] = bobjNone;
    //objects[x][y] = bobjPlayer;
    hero.x = x;
    hero.y = y;

    maybe_trigger_door(hero.x, hero.y + 1);
    maybe_trigger_door(hero.x, hero.y - 1);
    maybe_trigger_door(hero.x - 1, hero.y);
    maybe_trigger_door(hero.x + 1, hero.y);
    maybe_trigger_door(hero.x, hero.y);
}

void MutableState::player_action(PlayerAction action, Direction dir) {
    // First process player action
    switch (action) {
        case actNone:
            break;
        case actMove:
            player_move(dir);
            break;
        // case actOperate:
        //     player_operate(dir);
        //     break;
        case actShoot:
            player_shoot(dir, false);
            break;
        case actShootLaser:
            player_shoot(dir, true);
            break;
        case actBomb:
            player_bomb(dir);
            break;
        default:
            assert(false);
    }
}

void MutableState::player_move(Direction dir) {
    assert(hero.move_cooldown == 0);

    int x = hero.x, y = hero.y;
    x += dirx(dir);
    y += diry(dir);

    if (tile(x, y).hero_passable() == false)
        return;

    BasicObject &obj = objects[x][y];
    switch (obj.type) {
        case bobjNone:
        case bobjGreySlime:
            // OK to step on
            break;
        case bobjGlass:
        case bobjDoor:
            // Illegal move
            // (The door is not an autodoor if it's closed)
            return;
        case bobjBox:
        {
            // Try to push
            int x2 = x + dirx(dir);
            int y2 = y + diry(dir);
            if (tile(x2, y2).type == btilePit) {
                // destroy it
                obj.set(bobjNone);
                break;
            } else if (tile(x2, y2).box_passable() && objects[x2][y2].type == bobjNone) {
                // push
                obj.set(bobjNone);
                objects[x2][y2].set(bobjBox);
                break;
            } else {
                // can't push
                return;
            }
        }
        case bobjRedSlime1:
        case bobjRedSlime2:
            // Illegal move
            return;
        case bobjBlueSlime:
        case bobjGreenSlime:
            // Take damage
            hero.hp -= 1;
            kill(x, y);
            break;
        case bobjHealth:
            if (hero.hp < HERO_MAX_HP) {
                hero.hp = min(HERO_MAX_HP, hero.hp + obj.data);
                obj.set(bobjNone);
            }
            break;
        case bobjAmmo:
            hero.ammo += obj.data * 2;
            obj.set(bobjNone);
            break;
        case bobjLaserAmmo:
            hero.laser_ammo += obj.data;
            obj.set(bobjNone);
            break;
        case bobjBomb:
            hero.bombs += 1;
            obj.set(bobjNone);
            break;
        default:
            assert(false);

    }
    move_player_to(x, y);
    hero.move_cooldown = HERO_MOVE_COOLDOWN;
}

/*
void MutableState::player_operate(Direction dir) {
    assert(hero.move_cooldown == 0);

    int x = hero.x, y = hero.y;
    x += dirx(dir);
    y += diry(dir);

    if (tile(x, y).hero_passable() == false)
        return;
*/

void MutableState::player_shoot(Direction dir, bool laser) {
    assert(hero.shoot_cooldown == 0);
    if (laser) {
        assert(hero.laser_ammo);
        hero.laser_ammo -= 1;
        hero.shoot_cooldown = HERO_LASER_COOLDOWN;
    } else {
        assert(hero.ammo);
        hero.ammo -= 1;
        hero.shoot_cooldown = HERO_SHOOT_COOLDOWN;
    }

    int x = hero.x, y = hero.y;
    while (true) {
        x += dirx(dir);
        y += diry(dir);
        // This shouldn't happen, due to walls
        if (x < 0 || x == MAPWIDTH || y < 0 || y == MAPHEIGHT)
            break;

        if (tile(x, y).bullet_passable() == false)
            break;

        BasicObject &obj = objects[x][y];
        switch (obj.type) {
            case bobjNone:
            case bobjFire:
            case bobjAmmo:
            case bobjLaserAmmo:
            case bobjHealth:
            case bobjBomb:
                // Pass over
                break;
            case bobjGlass:
                // Breaks
                obj.set(bobjNone);
                return;
            case bobjGreySlime:
            case bobjBlueSlime:
            case bobjGreenSlime:
            case bobjRedSlime1:
                // Kills
                kill(x, y);
                if (laser)
                    // Keep going
                    break;
                else
                    return;
            case bobjRedSlime2:
                // Injures
                obj.type = bobjRedSlime1;
                // preserve obj.data
                if (laser)
                    // Keep going
                    break;
                else
                    return;
            case bobjDoor:
            case bobjBox:
                // Blocked
                return;
            default:
                assert(false);
        }
    }
}

void MutableState::player_bomb(Direction dir) {
    assert(hero.shoot_cooldown == 0);
    assert(hero.bombs);
    hero.bombs -= 1;
    hero.shoot_cooldown = HERO_BOMB_COOLDOWN;

    int x = hero.x, y = hero.y;
    for (int throw_dist = 5; throw_dist; throw_dist--) {
        int oldx = x, oldy = y;
        x += dirx(dir);
        y += diry(dir);
        // This shouldn't happen, due to walls
        if (x < 0 || x == MAPWIDTH || y < 0 || y == MAPHEIGHT)
            return;

        if (tile(x, y).bullet_passable() == false) {
            explode_bomb_at(oldx, oldy);
            return;
        }

        BasicObject &obj = objects[x][y];
        switch (obj.type) {
            case bobjNone:
            case bobjFire:
            case bobjAmmo:
            case bobjLaserAmmo:
            case bobjHealth:
            case bobjBomb:
            case bobjGreySlime:
            case bobjBlueSlime:
            case bobjGreenSlime:
            case bobjRedSlime1:
            case bobjRedSlime2:
                // Pass over
                break;
            case bobjGlass:
                // Breaks
                obj.set(bobjNone);
                explode_bomb_at(x, y);
                return;
            case bobjDoor:
            case bobjBox:
                // Blocked
                explode_bomb_at(oldx, oldy);
                return;
            default:
                assert(false);
        }
    }

    // Full distance throw
    if (tile(x, y).type == btilePit) {
        // in a pit
    } else {
        explode_bomb_at(x, y);
    }
}

void MutableState::explode_bomb_at(int x, int y) {
    // Flames spread out using a queue/BFS rather than recursion/DFS
    // to ensure a circular shape without having to do 

    struct BombedTile {
        int x, y, distleft;
        Direction lastdir;  // -1 if centre tile
    };

    queue<BombedTile> Q;
    // Add centre tile so it's processed (dir -1 prevents spread)
    Q.push({x, y, 50, INVALID});
    // Spread out
    for (int diri = 0; diri <= 7; diri++) {
        Direction dir = (Direction)diri;
        int x2 = x + dirx(dir), y2 = y + diry(dir);
        Q.push({x2, y2, 50 - dirdist10(dir), dir});
    }

    while (!Q.empty()) {
        BombedTile node = Q.front();
        Q.pop();

        // What do we have here?
        if (tile(node.x, node.y).fire_passable() == false) {
            // Includes pits
            continue;
        }

        BasicObject &obj = objects[node.x][node.y];
        switch (obj.type) {
            case bobjNone:
            case bobjHealth:
                // Cover floor, destroy health
                obj.set(bobjFire, min(15, 9 + node.distleft / 8));
                break;
            case bobjAmmo:
            case bobjLaserAmmo:
                // Flows over without destroying
                break;
            case bobjBomb:
                // Chain reaction
                obj.set(bobjNone);
                explode_bomb_at(node.x, node.y);
                break;
            case bobjFire:
                // Can't distinguish between flames from other paths and
                // preexisting flames, so take the max rather than adding.
                obj.set(bobjFire, max((int)obj.data, min(15, 9 + node.distleft / 8)));
                break;
            case bobjDoor:
            case bobjGlass:
            case bobjBox:
                // Blocked, no spreading
                continue;
            case bobjGreySlime:
            case bobjBlueSlime:
            case bobjGreenSlime:
            case bobjRedSlime1:
            case bobjRedSlime2:
                kill(node.x, node.y);
                node.distleft -= 7;
                break;
            default:
                assert(false);
        }

        // Continue spreading if not the centre tile
        // Can spread in 3 directions
        if (node.lastdir != -1) {
            for (int deviate = -1; deviate <= 1; deviate++) {
                Direction newdir = (Direction)((node.lastdir + deviate) & 7);
                int newdist = node.distleft - dirdist10(newdir);
                if (newdist > 0) {
                    int x2 = node.x + dirx(newdir), y2 = node.y + diry(newdir);
                    Q.push({x2, y2, newdist, newdir});
                }
            }
        }
    }

}


/*********************************** Time Stepping ****************************/

void MutableState::grey_slime_maybe_morph(BasicObject &obj, int x, int y) {
    // Check still grey
    if (obj.type != bobjGreySlime)
        return;

    BasicObject &adj_obj = objects[x][y];
    switch (adj_obj.type) {
        case bobjRedSlime1:
            obj.set(bobjRedSlime2, 15);
            break;
        case bobjBlueSlime:
        case bobjGreenSlime:
        case bobjRedSlime2:
            obj.set(adj_obj.type, 15);
            break;
        default:
            break;
    }
}

static const int slime_move_cooldowns[16] = {
    0,  // bobjNone
    0,  // unused
    0,  // bobjGreySlime
    4,  // bobjBlueSlime
    2,  // bobjGreenSlime
    3,  // bobjRedSlime1
    2,  // bobjRedSlime2
/* Unimplemented in g++ 4.7
    [bobjBlueSlime] = 4,
    [bobjGreenSlime] = 2,
    [bobjRedSlime1] = 3,
    [bobjRedSlime2] = 2,
*/
};

// Return true on success
bool MutableState::slime_try_move(int x, int y, Direction dir) {
    BasicObject &obj = objects[x][y];
    x += dirx(dir);
    y += diry(dir);

    if (!tile(x, y).enemy_passable())
        return false;

    if (x == hero.x && y == hero.y) {
        hero.hp -= 1;
        if (obj.type == bobjGreenSlime) {
            // Green slimes push you
            x += dirx(dir);
            y += diry(dir);
            if (tile(x, y).type == btilePit || tile(x, y).type == btileFloor) {
                hero.x = x;
                hero.y = y;
            }
            obj.set(bobjNone);
        } else if (obj.type == bobjBlueSlime) {
            obj.set(bobjNone);
        } else {
            // Red slimes
        }
        return true;
    }

    // See what's in front
    BasicObject &obj2 = objects[x][y];
    switch (obj2.type) {
        case bobjNone:
            // Move
            obj2.set(obj.type, slime_move_cooldowns[obj.type]);
            obj.set(bobjNone);
            return true;
        case bobjFire:
            if (obj.type == bobjBlueSlime && rng(3) == 0) {
                // Extra braindead, move onto it 33% of the time
                //obj2.data = max(0, obj2.data - 8);
                //if (obj2.data == 0)
                obj2.set(bobjNone);
                obj.set(bobjNone);
                return true;
            } else {
                // blocked
                return false;
            }
        case bobjBlueSlime:
        case bobjGreenSlime:
        case bobjRedSlime1:
        case bobjRedSlime2:
        {
            // Push the slime in front IF there's a space on the other side!
            x += dirx(dir);
            y += diry(dir);
            if (!tile(x, y).enemy_passable())
                return false;
            BasicObject &obj3 = objects[x][y];
            if (obj3.type != bobjNone)
                return false;

            // Push, and both get cooldown
            obj3.set(obj2.type, slime_move_cooldowns[obj2.type]); // or use obj.type?
            obj2.set(obj.type, slime_move_cooldowns[obj.type]); // or use obj.type?
            obj.set(bobjNone);
            return true;
        }
        default:
            return false;
    }
}

void MutableState::slime_logic(int x, int y) {
    BasicObject &obj = objects[x][y];
    switch (obj.type) {
        case bobjGreySlime:
            grey_slime_maybe_morph(obj, x, y + 1);
            grey_slime_maybe_morph(obj, x, y - 1);
            grey_slime_maybe_morph(obj, x + 1, y);
            grey_slime_maybe_morph(obj, x - 1, y);
            break;
        case bobjBlueSlime:
        case bobjGreenSlime:
        case bobjRedSlime1:
        case bobjRedSlime2:
        {
            // Decrement move cooldown
            if (--obj.data > 0)
                return;
            int xdiff = hero.x - x;
            int ydiff = hero.y - y;
            int dist = abs(xdiff) + abs(ydiff);
            /*
            if (abs(xdiff) >= abs(ydiff)) {
                if (slime_try_move(sign(xdiff), 0))
                    return;
            }
            */
            Direction dir = xy_to_dir(xdiff, ydiff);
            if (dist > 5) {
                // Spread out randomly
                if (!slime_try_move(x, y, Direction((dir + rng(3) - 1 + 8) & 7)))
                    slime_try_move(x, y, dir);
            } else {
                // Directest first
                if (!slime_try_move(x, y, dir)) //, sign(xdiff), sign(ydiff)))
                    if (!slime_try_move(x, y, Direction((dir + 1) & 7)))
                        slime_try_move(x, y, Direction((dir + 7) & 7));
            }
            break;
        }
        default:
            assert(false);
    }
}

void MutableState::step(PlayerAction action, Direction action_dir) {
    ticknum += 1;
    prng_state = (int)ticknum * 100000007 + (ticknum ^ 0x21f3091e);

    hero.move_cooldown = max(0, hero.move_cooldown - 1);
    hero.shoot_cooldown = max(0, hero.shoot_cooldown - 1);
    if (lost() || won())
        action = actNone;
    player_action(action, action_dir);

    for (int x = 0; x < MAPWIDTH; x += 1) {
        for (int y = 0; y < MAPHEIGHT; y += 1) {
            BasicObject &obj = objects[x][y];
            switch (obj.type) {
                case bobjFire:
                    if (rng(4) == 0) {
                        if (!obj.decrement(1)) {
                            obj.set(bobjNone);
                        }
                    }
                    break;
                case bobjNone:
                case bobjGlass:
                case bobjDoor:
                case bobjBox:
                case bobjAmmo:
                case bobjLaserAmmo:
                case bobjBomb:
                case bobjHealth:
                    break;
                case bobjGreySlime:
                case bobjBlueSlime:
                case bobjGreenSlime:
                case bobjRedSlime1:
                case bobjRedSlime2:
                    slime_logic(x, y);
                    break;
                default: //case bobjUnused:
                    //fprintf(stderr, "%s: Illegal object type\n", __PRETTY_FUNCTION__);
                    assert(false);
            }
        }
    }

    // Other processes

    BasicObject &obj = objects[hero.x][hero.y];
    if (obj.type == bobjFire) {
        hero.hp -= 1;
        if (!obj.decrement(9))
            obj.set(bobjNone);
    }

    // Don't need to process autodoors; that's handled in move_player_to()
    // Standing on the exit also causes won() to return true, no other effect required

    // Clear prng state to help hashing
    prng_state = 0;
}
