/* Map generation
 */


#include "state.hpp"
#include "global.hpp"
#include <algorithm>
#include <assert.h>

using namespace std;

const char* const level_names[] = {
    "Floor 1: The cutting floor",
    "Floor 2: Maintenance deck",
    "Floor 3: Breeding level",
    "Floor 4: Administrative offices",
};

// BasicTileEnum to tile number mapping, DoomRPG tileset 0
const static int btile_to_tile[] = {
    1 * 16 + 2,  // btileFloor,
    1 * 16 + 15, // btileWall,
    0 * 16 + 11, // btilePit,
    2 * 16 + 1,  // btileAutodoor,
    2 * 16 + 1,  // btileRemoteDoor,  // data holds door num
    5 * 16 + 2,  // btileDoorSwitch,  // data holds door num. Impassable
    2 * 16 + 2,  // btileExit,
};


void GraphicalState::generate_map() {
    delete imstate;
    delete mustate;
    imstate = new ImmutableState();

    imstate->level_num = 0;
    imstate->level_name = level_names[imstate->level_num];

    // Initialise player
    MutableState::Hero &hero = imstate->initial_state.hero;
    // x, y set in generate_toplevel()
    hero.x = 1;
    hero.y = 2;
    hero.hp = HERO_MAX_HP;
    hero.ammo = 0;
    hero.laser_ammo = 0;
    hero.bombs = 0;
    player_dir = UP;

    fill(&tilemap[0][0][0], &tilemap[0][0][0] + MAPWIDTH * MAPHEIGHT * 2, 0);
    fill(&stylemap[0][0], &stylemap[0][0] + MAPWIDTH * MAPHEIGHT, stylePlain);
    // Generate stuff
    generate_toplevel();

    mustate = new MutableState(imstate);
}

void GraphicalState::generate_toplevel() {
    // The top row of the map is wasted in order to show the top of the wall
    // at the top.
    place_room(XY{0, 1}, XY{MAPWIDTH, MAPHEIGHT - 1}, stylePlain);

    printf("%s\n", __PRETTY_FUNCTION__);
}

void GraphicalState::put_floor(int x, int y, RoomStyle style) {
    assert(x >= 0 && x < MAPWIDTH && y >= 0 && y < MAPHEIGHT);
    tilemap[x][y][0] = btile_to_tile[btileFloor];
    tilemap[x][y][1] = 0;
    stylemap[x][y] = style;
    imstate->tiles[x][y].set(btileFloor);
}

void GraphicalState::put_wall(int x, int y, RoomStyle style) {
    assert(x >= 0 && x < MAPWIDTH && y >= 1 && y < MAPHEIGHT);
    tilemap[x][y][0] = btile_to_tile[btileWall];
    tilemap[x][y - 1][1] = 16 * 8 + 6;
    stylemap[x][y] = style;
    imstate->tiles[x][y].set(btileWall);
}

void GraphicalState::floor_rect(XY topleft, XY size, RoomStyle style) {
    for (int x = topleft.x; x < topleft.x + size.w; x += 1) {
        for (int y = topleft.y; y < topleft.y + size.h; y += 1) {
            put_floor(x, y, style);
        }
    }
}

void GraphicalState::place_room(XY topleft, XY size, RoomStyle style) {
    assert(topleft.y >= 1);
    floor_rect(topleft + XY{1, 1}, size - XY{2, 2}, style);

    for (int x = topleft.x; x < topleft.x + size.w; x += 1) {
        put_wall(x, topleft.y, style);
        put_wall(x, topleft.y + size.h - 1, style);
    }

    for (int y = topleft.y; y < topleft.y + size.h; y += 1) {
        put_wall(topleft.x, y, style);
        put_wall(topleft.x + size.w - 1, y, style);
    }
}

// void GraphicalState::place_enemy(int x, int y, BasicObjectEnum type) {
    
//     switch (type) {
//         case bobjGreySlime:
// }

