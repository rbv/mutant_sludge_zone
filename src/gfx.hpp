#ifndef GFX_HPP
#define GFX_HPP

#include <SDL.h>
#include "types.hpp"
#include "nfont/NFont.h"

// Owns the SDL_Texture it is given
class SpriteSheet {
    SDL_Texture *_tex;
    SDL_Rect _bounds;  // size of the texture
    int _frames_per_row;
    int _frames_per_column;
    XY _frameoffset;  // Position of origin from top left corner; should be nonnegative, so the origin

public:
    XY _framesize;  // Size of a single frame, not the whole image

    // Create a dummy that does nothing when drawn
    SpriteSheet() : _tex(NULL) {}
    // frameoffset is the 
    SpriteSheet(SDL_Texture *texture, int frames_per_row = 1, int frames_per_column = 1, XY frameoffset = {0, 0});

    ~SpriteSheet() { SDL_DestroyTexture(_tex); }

    //void setBounds(const SDL_Rect &rect) { bounds = rect; }

    // Draw a frame.
    // Position on screen in pixels
    void drawAt(XYd pos, int frame = 0, double scale = 1.);
    void drawAt(XY pos, int frame = 0, double scale = 1.) { drawAt(XYd(pos), frame, scale); }
};



// Print the last SDL error to stderr
void SDLerr(const char *msg, ...);

SDL_Surface *createSurface(int w, int h);
SDL_Surface *createSurface(XY size);
SDL_Surface *loadSurface(const char *name, bool keyed = false);
SDL_Texture *loadTexture(const char *name, bool keyed = false);
SDL_Texture *toTexture(SDL_Surface *surf);


/************************************* Text ***********************************/


char *formatText(const char *format, ...);
SpriteSheet *renderText(NFont &font, int width, const char *text);


#endif
