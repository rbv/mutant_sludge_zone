/*
  Drawing routines
 */

#include "math.h"
#include "global.hpp"
#include "gfx.hpp"


void SDLerr(const char *msg, ...) {
    static char buf[2048];
    va_list lst;
    va_start(lst, msg);
    vsnprintf(buf, 2048, msg, lst);
    va_end(lst);
    fprintf(stderr, "%s; SDL error: %s\n", buf, SDL_GetError());
}

/*********************************** Loading **********************************/


SDL_Surface *createSurface(int w, int h) {
    return SDL_CreateRGBSurface(0, w, h, 32, 0xff000000, 0xff0000, 0xff00, 0xff);
}

SDL_Surface *createSurface(XY size) {
    return createSurface(size.w, size.h);
}

SDL_Surface *loadSurface(const char *name, bool keyed) {
    char buf[256];
    snprintf(buf, 256, "res/%s", name);
    SDL_Surface *ret = SDL_LoadBMP(buf);
    if (!ret) {
        SDLerr("Could not load %s", buf);
        return NULL;
    }
    if (keyed) {
        //int col = SDL_MapRGB(ret->format, 255, 0, 255);
        int col = 0;
        SDL_SetColorKey(ret, true, col);
    }

    // snprintf(buf, 256, "res/debug_%s", name);
    // SDL_SaveBMP(ret, buf);

    return ret;
}

SDL_Texture *loadTexture(const char *name, bool keyed) {
    SDL_Surface *tempsurf = loadSurface(name, keyed);
    if (!tempsurf) {
        return NULL;
    }
    SDL_Texture *ret = SDL_CreateTextureFromSurface(gstate.renderer, tempsurf);
    SDL_FreeSurface(tempsurf);
    if (!ret)
        SDLerr("Could not create texture from %s", name);
    return ret;
}

SDL_Texture *toTexture(SDL_Surface *surf) {
    SDL_Texture *ret = SDL_CreateTextureFromSurface(gstate.renderer, surf);
    SDL_FreeSurface(surf);
    if (!ret)
        SDLerr("Could not create texture from surface");
    return ret;
}


/********************************* SpriteSheet ********************************/


SpriteSheet::SpriteSheet(SDL_Texture *texture, int frames_per_row, int frames_per_column, XY frameoffset) {
    _tex = texture;
    _bounds = SDL_Rect{0, 0, 0, 0};
    if (SDL_QueryTexture(_tex, NULL, NULL, &_bounds.w, &_bounds.h))
        SDLerr("Couldn't query texture");
    _frames_per_row = frames_per_row;
    _frames_per_column = frames_per_column;
    _framesize.w = _bounds.w / frames_per_row;
    _framesize.h = _bounds.h / frames_per_column;
    _frameoffset = frameoffset;
}

void SpriteSheet::drawAt(XYd pos, int frame, double scale) {
    if (!_tex)
        return;
    int framex = frame % _frames_per_row;
    int framey = frame / _frames_per_row;
    SDL_Rect srcrect{_framesize.w * framex, _framesize.h * framey, _framesize.w, _framesize.h};
    SDL_Rect destrect{int(pos.x) - _frameoffset.x, int(pos.y) - _frameoffset.y, int(_framesize.w * scale), int(_framesize.h * scale)};
    SDL_RenderCopy(gstate.renderer, _tex, &srcrect, &destrect);
}


/************************************* Text ***********************************/


char *formatText(const char *format, ...) {
    static char retbuf[2048];
    va_list lst;
    va_start(lst, format);
    vsnprintf(retbuf, 2048, format, lst);
    va_end(lst);
    return retbuf;
}


// Draw a string. If size is not NULL, fill with size of output
SDL_Texture *_renderText(NFont &font, int width, XY *size, const char *text) {
    int height = font.getColumnHeight(width, text);

    SDL_Texture *tex = NULL;
    SDL_Surface *textsurf = createSurface(width, height);
    SDL_Rect rect = font.drawColumn(textsurf, 0, 0, width, text);
    tex = toTexture(textsurf);
    if (size)
        *size = XY(rect.w, rect.h);
    return tex;
}

SpriteSheet *renderText(NFont &font, int width, const char *text) {
    XY size;
    SDL_Texture *texture = _renderText(font, width, &size, text);
    SpriteSheet *ret;
    ret = new SpriteSheet(texture);
    // What's this for?
    //ret->setBounds(SDL_Rect{0, 0, size.w, size.h});
    return ret;
}

