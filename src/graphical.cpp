/* The graphical game.
 */

#include "state.hpp"
#include "gfx.hpp"
#include "global.hpp"
#include <vector>
#include <algorithm>


GraphicalState::GraphicalState() {
    imstate = NULL;
    mustate = NULL;
    hud = NULL;
}

GraphicalState::~GraphicalState() {
    delete imstate;
    delete mustate;
    delete hud;
}


void MapSprite::draw() {
    spritesheet->drawAt(gstate.map_pos + XY{x, y + 10}, frame);
}

void GraphicalState::draw() {
    draw_map(0);

    // Make use of sorted
    vector<MapSprite> objects;
    for (int y = 0; y < MAPHEIGHT; y += 1) {
        for (int x = 0; x < MAPWIDTH; x += 1) {
        }
    }
    // Frame 4
    objects.push_back(MapSprite{mustate->hero.x * 20, mustate->hero.y * 20, 0, 4, gstate.spritesets});

    sort(objects.begin(), objects.end());

    for (int i = 0; i < (int)objects.size(); i++) {
        objects[i].draw();
    }

    draw_map(1);

    update_hud();
    hud->drawAt(gstate.hud_pos, 0, gstate.scale);
}

void GraphicalState::update_hud() {
    delete hud;

    SDL_Surface *surf = createSurface(gstate.hud_size);

    // void drawtext(const char *text) {
    //     font.drawColumn(surf, x, y, 300, text);
    // }

    //drawtext(20, 5, formatText("HP: %d", mustate->hero.hp));
    gstate.guifont.drawColumn(surf, 20, 5, 300, formatText("HP: %d", mustate->hero.hp));
    gstate.guifont.drawColumn(surf, 130, 5, 300, formatText("Ammo: %d", mustate->hero.ammo));
    gstate.guifont.drawColumn(surf, 130, 25, 300, "(Z+dir to fire)");
    gstate.guifont.drawColumn(surf, 290, 5, 300, formatText("Laser shots: %d", mustate->hero.laser_ammo));
    gstate.guifont.drawColumn(surf, 290, 25, 300, "(X+dir to fire)");
    gstate.guifont.drawColumn(surf, 460, 5, 300, formatText("Bombs: %d", mustate->hero.bombs));
    gstate.guifont.drawColumn(surf, 460, 25, 300, "(C+dir to throw)");
    gstate.guifont.drawColumn(surf, 740, 5, 300, imstate->level_name);

    // Debug
    gstate.guifont.drawColumn(surf, 710, 25, 300, formatText("Tick: %d", mustate->ticknum));

    hud = new SpriteSheet(toTexture(surf));
}

void GraphicalState::draw_map(int layer) {
    // Pixel position of topleft viewport corner
    //XYd cameraOffset = gstate.scale * (gstate.camera) - XYd(gstate.screensize) * 0.5;

    // for (double x = 0; x < gstate.screensize.w; x += tiles->_framesize.w) {
    //     for (double y = 0; y < gstate.screensize.h; y += tiles->_framesize.h) {
    //         gstate.tilesets.tiles->drawAt(XYd{x, y}, 0, gstate.scale);
    //     }
    // }

    SpriteSheet &tileset = *gstate.tileset0;

    //printf("%s scale %f framesize %d %d\n", __PRETTY_FUNCTION__, gstate.scale, tileset._framesize.w, tileset._framesize.h);

    for (int y = 0; y < MAPHEIGHT; y += 1) {
        for (int x = 0; x < MAPWIDTH; x += 1) {
            XYd drawpos = {(double)x * tileset._framesize.w, (double)y * tileset._framesize.h};
            drawpos *= gstate.scale;
            int tile = tilemap[x][y][layer];
            if (tile) {
                //printf("%s %d %d\n", __PRETTY_FUNCTION__, x, y);
                //cout << " tile at " << drawpos << nl;
                tileset.drawAt(gstate.map_pos + drawpos, tile, gstate.scale);
            }
        }
    }

}

