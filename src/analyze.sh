#!/bin/sh

rm *.d nfont/*.d
make clean
scan-build -o reports --use-analyzer /usr/bin/clang make -j4
