#!/bin/sh
for file in ressrc/*.bmp ressrc/*.png; do
    ofile=$(basename "$file" | sed -e 's/png/bmp/')
    #echo "$ofile"
    # SDL2 can't load compressed bmps
    # can add BMP3:
    convert "$file" -compress none BMP3:"res/$ofile"
done

